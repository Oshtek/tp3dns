#!/usr/bin/env bash

## Arrêt des services DNS pour chaque machine
himage dwikiorg killall -9 -q named 
himage diutre killall -9 -q named
himage drtiutre killall -9 -q named  
himage dorg killall -9 -q named 
himage sorg killall -9 -q named
himage dre killall -9 -q named
himage dza killall -9 -q named 
himage aRootServer killall -9 -q named
himage bRootServer killall -9 -q named 
himage cRootServer killall -9 -q named
himage deduza killall -9 -q named
himage duniveduza killall -9 -q named
himage dcomza killall -9 -q named
himage dfacebookcomza killall -9 -q named

## configuration de rt.iut.re 
# configuration du serveur drtiutre

himage drtiutre mkdir -p /etc/named
hcp drtiutre/named.conf drtiutre:/etc/.
hcp drtiutre/* drtiutre:/etc/named/.
himage drtiutre rm /etc/named/named.conf 

## configuration de wiki.org 
# configuration du serveur dns

himage dwikiorg mkdir -p /etc/named
hcp dwikiorg/named.conf dwikiorg:/etc/.
hcp dwikiorg/* dwikiorg:/etc/named/.
himage dwikiorg rm /etc/named/named.conf 

## configuration de iut.re 
# configuration du serveur dns

himage diutre mkdir -p /etc/named
hcp diutre/named.conf diutre:/etc/.
hcp diutre/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf 

## configuration de .org
# configuration du serveur dns maître

himage dorg mkdir -p /etc/named
hcp dorg/named.conf dorg:/etc/.
hcp dorg/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf 

#configuration du serveur dns esclave

himage sorg mkdir -p /etc/named
hcp sorg/named.conf sorg:/etc/.
hcp sorg/* sorg:/etc/named/.

## configuration de .re 
# configuration du serveur dns

himage dre mkdir -p /etc/named
hcp dre/named.conf dre:/etc/.
hcp dre/* dre:/etc/named/.
himage dre rm /etc/named/named.conf 

## configuration de .za
# configuration du serveur dns

himage dza mkdir -p /etc/named
hcp dza/named.conf dza:/etc/.
hcp dza/* dza:/etc/named/.
himage dza rm /etc/named/named.conf

## configuration de com.za
# configuration du serveur dcomza

himage dcomza mkdir -p /etc/named
hcp dcomza/named.conf dcomza:/etc/.
hcp dcomza/* dcomza:/etc/named/.
himage dcomza rm /etc/named/named.conf

## configuration de facebook.com.za
# configuration du serveur dfacebookcomza

himage dfacebookcomza mkdir -p /etc/named
hcp dfacebookcomza/named.conf dfacebookcomza:/etc/.
hcp dfacebookcomza/* dfacebookcomza:/etc/named/.
himage dfacebookcomza rm /etc/named/named.conf

## configuration de edu.za
# configuration du serveur deduza

himage deduza mkdir -p /etc/named
hcp deduza/named.conf deduza:/etc/.
hcp deduza/* deduza:/etc/named/.
himage deduza rm /etc/named/named.conf

## configuration de univedu.za
# configuration du serveur duniveduza

himage duniveduza mkdir -p /etc/named
hcp duniveduza/named.conf duniveduza:/etc/.
hcp duniveduza/* duniveduza:/etc/named/.
himage duniveduza rm /etc/named/named.conf

## configuration de .
# configuration du serveur aRootServer

himage aRootServer mkdir -p /etc/named
hcp aRootServer/named.conf aRootServer:/etc/.
hcp aRootServer/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf 

# configuration du serveur bRootServer

himage bRootServer mkdir -p /etc/named
hcp bRootServer/named.conf bRootServer:/etc/.
hcp bRootServer/* bRootServer:/etc/named/.

# configuration du serveur cRootServer

himage cRootServer mkdir -p /etc/named
hcp cRootServer/named.conf cRootServer:/etc/.
hcp cRootServer/* cRootServer:/etc/named/.

## configuration de pc1 
# resolv.conf

hcp pc1/resolv.conf pc1:/etc/.
hcp pc2/resolv.conf pc2:/etc/.

## Lancement des services DNS pour chaque machine

himage dwikiorg named -c /etc/named.conf
himage diutre named -c /etc/named.conf
himage drtiutre named -c /etc/named.conf
himage dorg named -c /etc/named.conf
himage sorg named -c /etc/named.conf
himage dre named -c /etc/named.conf
himage dza named -c /etc/named.conf
himage aRootServer named -c /etc/named.conf
himage bRootServer named -c /etc/named.conf
himage cRootServer named -c /etc/named.conf
himage deduza named -c /etc/named.conf
himage duniveduza named -c /etc/named.conf
himage dcomza named -c /etc/named.conf
himage dfacebookcomza named -c /etc/named.conf
